Rails.application.routes.draw do
  root to: 'seats#index'
  resource :seats, only: [:update]
  get '/seats', to: 'seats#index'
  get '/seats/validate', to: 'seats#validate'
end
