class CustomerMailer < ApplicationMailer
  def booking_confirmation(user)
    @user = user
    mail to: user.email, subject: "Confirmation of Booking"
  end
end
