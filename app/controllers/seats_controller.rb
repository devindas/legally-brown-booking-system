class SeatsController < ApplicationController
  def index
  end

  def update
    block = params[:block].to_s.downcase
    row = params[:row].to_i
    first_seat = params[:first_seat].to_i
    last_seat = params[:last_seat].to_i
    first_name = params[:first_name].to_s
    last_name = params[:last_name].to_s
    email = params[:email].to_s
    contact = params[:contact].to_i

    seats = find_seats(block, row, first_seat, last_seat)
    unless customer = Customer.find_by(email: email)
      customer = Customer.create(first_name: first_name, last_name: last_name, email: email, contact: contact)
      seats.each do |seat|
        seat.update_attributes(customer_id: customer.id, booked: true) unless seat.booked?
      end
      # CustomerMailer.booking_confirmation(customer).deliver_now
    end

    respond_to do |format|
      format.js {render inline: "location.reload();" }
    end
  end

  def validate
    block = params[:block].to_s.downcase
    row = params[:row].to_i
    first_seat = params[:first_seat].to_i
    last_seat = params[:last_seat].to_i

    @booked_seats = []
    seats = find_seats(block, row, first_seat, last_seat)
    seats.each do |seat|
      @booked_seats.push(seat.column) if seat.booked?
    end
    @booked_seats = @booked_seats.join(", ")

  end
  def find_seats(block, row, seat_1, seat_2)
    seats = []
    (seat_1..seat_2).each do |seat_num|
      seat = Seat.find_by(block: block, row: row, column: seat_num)
      seats.push(seat) unless seats.include? seat
    end
    seats
  end
end
