module SeatsHelper
  def find_rows(blk)
    rows = []
    Seat.where(block: blk).each do |seat|
      rows.push(seat.row) unless rows.include? seat.row
    end
    rows[]
  end
end
