#Seat Booking System

This is a seating booking app created with Ruby on Rails. The purpose of this app is to create a fast, responsive UI to allow a single user to manage the bookings of multiple seats for an event.