# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
block_A = [30, [26]]
block_B = [30, [16]]
block_C = [30, [16]]
block_D = [15, [3,6,9,12,16,20,24,28,31,33,37,40,44,49,49]]
block_E = [15, [3,6,9,12,16,20,24,28,31,33,37,40,44,49,49]]
block_F = [21, [18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,17,16,14,13]]
block_G = [27, [15]]
block_H = [22, [12,13,15,16,18,20,21,23,26,24,23,21,19,18,16,15,13,11,9,7,6,4]]
block_I = [22, [12,13,15,16,18,20,21,23,26,24,23,21,19,18,16,15,13,11,9,7,6,4]]
block_J = [27, [15]]
block_K = [21, [18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,17,16,14,13]]

blocks = [['a',[30, [26]]], ['b',[30, [16]]], ['c', [30, [16]]], ['d', [15, [3,6,9,12,16,20,24,28,31,33,37,40,44,49,49]]], ['e', [15, [3,6,9,12,16,20,24,28,31,33,37,40,44,49,49]]], ['f', [21, [18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,17,16,14,13]]], ['g',[27, [15]]], ['h',[22, [12,13,15,16,18,20,21,23,26,24,23,21,19,18,16,15,13,11,9,7,6,4]]], ['i',[22, [12,13,15,16,18,20,21,23,26,24,23,21,19,18,16,15,13,11,9,7,6,4]] ], ['j', [27, [15]]], ['k',[21, [18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,17,16,14,13]]]]

blocks.each_with_index { |block , index |
  block[1][0].times { |row|
    new_row = row
    new_row = 0 if block[1][1].size < 2
    block[1][1][new_row].times { |column|
      puts "#{block[0]} - row: #{row + 1} - column: #{column + 1}"
      Seat.create(block: block[0], row: (row + 1 ), column: (column + 1))
    }
  }
}
