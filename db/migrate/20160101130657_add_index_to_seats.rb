class AddIndexToSeats < ActiveRecord::Migration
  def change
    add_index :seats, [:block, :row, :column]
  end
end
