class CreateSeats < ActiveRecord::Migration
  def change
    create_table :seats do |t|
      t.string :block
      t.integer :row
      t.integer :column
      t.integer :seat_type
      t.boolean :booked, default: false
      t.belongs_to :customer
    end
  end
end
