class Add2IndexToSeats < ActiveRecord::Migration
  def change
    add_index :seats, :block
    add_index :seats, :row
    add_index :seats, :column
  end
end
